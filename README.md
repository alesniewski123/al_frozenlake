# FrozenLake8x8
![](sample_game_result.gif)

## Introduction
I have solved FrozenLake8x8 problem by using "Value Iteration" algorithm. Firstly, I was trying to resolve the task using the "Q-learning" algorithm. This algorithm worked well with the 4x4 version of the FrozenLake problem, but it was less effective with the 8x8 version. It might be related to the fact, that in the case of that environment we know all probabilities a priori. In these cases, dynamic programming algorithms, such as "Value Iteration" are able to perform well. "Q-learning" is based on learning by doing and it can optimize policy without knowing the whole model. It occurred that it wasn't neceserry in that game.

I have found on the internet that other people have also resolved that game by the "Value Iteration" algorithm. However, in the algorithms that I have found online, there were errors and some imperfections. The best model that I have found on the internet is performing with 75% of success. In my case, I have achieved a result of approximately 86% of success.

## How to use?
The task has been resolved using Jupyter Notebook and it can be run line by line or as a whole document. Before running the code please check if you have the 'FrozenLake8x8-v1' environment installed on your machine. It is available in the newest version of the gym library.

## Result
The game has been run 10000 times

Successes:  8632

Losses:  1368

% of sucesses:  86.32 %

Average steps to win:  81.0

